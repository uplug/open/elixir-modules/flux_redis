# Flux Redis

[![pipeline status](https://gitlab.com/uplug/open/elixir-modules/flux_redis/badges/master/pipeline.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_redis/commits/master)
[![coverage report](https://gitlab.com/uplug/open/elixir-modules/flux_redis/badges/master/coverage.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_redis/commits/master)

Integrate [Redis](https://redis.io/) to Elixir projects.

It uses `Redix`, check their documentation to understand how this library
request commands to Redis.

## Usage

Add [Flux Redis](https://hex.pm/packages/flux_redis) as a dependency in your
`mix.exs` file:

```elixir
def deps do
  [{:flux_redis, "~> 0.0.2"}]
end
```

`FluxRedis.HashManager` describes how to use Redis as json storage.

`FluxRedis.PipelineManager` describes how to execute multiple commands in a
single request.

### Application Configuration

```elixir
import Config

config :flux_redis,
  connection: [
    uri: "redis://redis",
    timeout: 5_000,
    sync_connect: true,
    exit_on_disconnection: false,
    backoff_initial: 1_000,
    backoff_max: 10_000
  ],
  hash_storages: []
```

- `:connection` - Set the Redis connection. Accepts `t:keyword/0`. Options are:

  - `:uri` - The Redis URI. Accepts `t:String.t/0`. Defaults to
    `redis://redis`. More information at `t:FluxRedis.redis_uri/0`.

  - `:timeout` - Milliseconds to wait for a connection. Defaults to `5_000`.
    Accepts `t:integer/0`.

  - `:sync_connect` - If `false`, it will not hold while starting the child
    process. Defaults to `true`. Accepts `t:boolean/0`.

  - `:exit_on_disconnection` - If `true`, it will exit and trigger error on
    connection failure. Defaults to `false`. Accepts `t:boolean/0`.

  - `:backoff_initial` - Initial milliseconds to wait before reattempting
    connection. Defaults to `1_000`. Accepts `t:integer/0`.

  - `:backoff_max` - Maximum milliseconds to wait before reattempting
    connection. The backoff will increase by a ratio of `1.5` until it reaches
    the maximum backoff. Defaults to `10_000`. Accepts `t:integer/0`.

- `:hash_storages` - Set the JSON storage name and the key `t:atom/0` which
  will generate the redis hash field name. Accepts `t:keyword/0`. Defaults to
  empty `t:keyword/0`. An example of definition is:

  ```elixir
  hash_storages: [
    cars: [:brand, :model],
    users: :id
  ]
  ```

  - Every key defined in `:hash_storage` will be used with the EXACT sequence
    to retrieve a JSON element or to create a redis hash field.

  - The hash field name is set by joining the value of each key with `|`
    separator.

    - From the example: users hash field names will be defined as `<user_id>`
      and cars hash field names will be defined as `<car_brand>|<car_model>`.
