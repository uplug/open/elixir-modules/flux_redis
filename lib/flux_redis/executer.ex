defmodule FluxRedis.Executer do
  @moduledoc false

  alias FluxRedis.ErrorHandler

  @type command_opts :: list({:no_reply?, boolean} | {:timeout, integer | :infinity})
  @type pipeline_opts ::
          list(
            {:as_transaction?, boolean}
            | {:no_reply?, boolean}
            | {:timeout, integer | :infinity}
          )

  @type command :: list
  @type parser :: (redix_data -> any)

  @type redix_data :: nil | String.t() | integer | Redix.Error.t() | list(redix_data)

  @spec execute_command!({command, parser}, command_opts) :: any
  def execute_command!({command, parser}, opts \\ []) do
    case do_execute_command!(command, opts) do
      :ok -> :ok
      result -> parser.(result)
    end
  end

  @spec execute_pipeline!({list(command), list(parser)}, pipeline_opts) :: list(any)
  def execute_pipeline!({commands, parsers}, opts \\ []) do
    {as_transaction?, opts} = Keyword.pop(opts, :as_transaction?, true)
    {no_reply?, opts} = Keyword.pop(opts, :no_reply?, false)

    commands
    |> do_execute_pipeline!(as_transaction?, no_reply?, opts)
    |> handle_pipeline_results!(parsers)
  end

  @spec execute_pipeline({list(command), list(parser)}, pipeline_opts) ::
          {:ok, list(any)} | {:error, Error.t()}
  def execute_pipeline({commands, parsers}, opts \\ []) do
    {as_transaction?, opts} = Keyword.pop(opts, :as_transaction?, true)
    {no_reply?, opts} = Keyword.pop(opts, :no_reply?, false)

    case do_execute_pipeline!(commands, as_transaction?, no_reply?, opts) do
      :ok -> :ok
      results -> {:ok, handle_pipeline_results(results, parsers)}
    end
  rescue
    error -> {:error, error}
  end

  defp do_execute_command!(command, opts) do
    {no_reply?, opts} = Keyword.pop(opts, :no_reply?)

    if no_reply? == true do
      Redix.noreply_command!(:redis, command, opts)
    else
      Redix.command!(:redis, command, opts)
    end
  rescue
    error -> reraise ErrorHandler.handle(error), __STACKTRACE__
  end

  defp do_execute_pipeline!(commands, as_transaction?, no_reply?, opts) do
    case {as_transaction?, no_reply?} do
      {_as_transaction?, true} -> Redix.noreply_pipeline!(:redis, commands, opts)
      {true, _no_reply?} -> Redix.transaction_pipeline!(:redis, commands, opts)
      {false, _no_reply?} -> Redix.pipeline!(:redis, commands, opts)
    end
  rescue
    error -> reraise ErrorHandler.handle(error), __STACKTRACE__
  end

  defp handle_pipeline_results!(:ok, _parsers) do
    :ok
  end

  defp handle_pipeline_results!(results, parsers) do
    results
    |> Enum.zip(parsers)
    |> Enum.map(&handle_pipeline_result!/1)
  end

  defp handle_pipeline_results(results, parsers) do
    results
    |> Enum.zip(parsers)
    |> Enum.map(&handle_pipeline_result/1)
  end

  defp handle_pipeline_result!({result, parser}) do
    if Exception.exception?(result) do
      raise ErrorHandler.handle(result)
    else
      parser.(result)
    end
  end

  defp handle_pipeline_result({result, parser}) do
    if Exception.exception?(result) do
      ErrorHandler.handle(result)
    else
      parser.(result)
    end
  end
end
