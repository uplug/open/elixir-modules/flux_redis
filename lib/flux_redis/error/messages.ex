defmodule FluxRedis.Error.Messages do
  @moduledoc false

  alias FluxRedis.Error

  @type opts :: Error.opts()

  @spec get(atom, opts) :: String.t()
  def get(reason, opts \\ []) do
    message = Map.get(messages(), reason, fn _opts -> "" end)
    message.(opts)
  end

  defp key(name, opts) do
    case Keyword.get(opts, :format, :elixir) do
      :elixir -> "%{#{name}}"
      :python -> "%(#{name})s"
    end
  end

  defp messages do
    %{
      decode_failed: fn _opts ->
        "payload is not JSON"
      end,
      encode_failed: fn _opts ->
        "data cannot be converted to JSON"
      end,
      key_not_found: fn opts ->
        "data does not have key '#{key(:key, opts)}'"
      end,
      not_found: fn _opts ->
        "data not found"
      end,
      request_failed: fn _opts ->
        "request failed"
      end
    }
  end
end
