defmodule FluxRedis.ErrorHandler do
  @moduledoc false

  alias FluxRedis.Error

  @type redix_error :: atom | Redix.ConnectionError.t() | Redix.Error.t()

  @spec handle(redix_error) :: Error.t()
  def handle(error) do
    Error.build(:request_failed, detail: error)
  end
end
