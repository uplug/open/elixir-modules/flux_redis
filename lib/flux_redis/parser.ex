defmodule FluxRedis.Parser do
  @moduledoc false

  alias FluxRedis.Error

  @spec decode_json!(String.t()) :: map
  def decode_json!(payload) do
    Jason.decode!(payload, keys: :atoms)
  rescue
    _error -> reraise Error.build(:decode_failed, payload: payload), __STACKTRACE__
  end

  @spec encode_to_json!(map) :: String.t()
  def encode_to_json!(data) do
    Jason.encode!(data)
  rescue
    _error -> reraise Error.build(:encode_failed, data: data), __STACKTRACE__
  end
end
