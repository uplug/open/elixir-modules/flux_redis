defmodule FluxRedis.Config do
  @moduledoc false

  alias FluxRedis.Config.Connection

  @type hash :: atom

  @spec connection :: Connection.t()
  def connection do
    :flux_redis
    |> Application.get_env(:connection, [])
    |> Connection.create()
  end

  @spec hash_keys(hash) :: list(atom)
  def hash_keys(hash) do
    keys =
      :flux_redis
      |> Application.get_env(:hash_storages, [])
      |> Keyword.get(hash, [])

    if is_list(keys) do
      keys
    else
      [keys]
    end
  end
end
