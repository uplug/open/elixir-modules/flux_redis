defmodule FluxRedis.Config.Connection do
  @moduledoc false

  alias FluxRedis.Config.Connection

  @type t :: %Connection{
          backoff_initial: integer,
          backoff_max: integer,
          exit_on_disconnection: boolean,
          name: atom,
          sync_connect: boolean,
          timeout: boolean,
          uri: String.t()
        }

  defstruct backoff_initial: 1_000,
            backoff_max: 10_000,
            exit_on_disconnection: false,
            name: :redis,
            sync_connect: true,
            timeout: 5_000,
            uri: "redis://redis"

  @spec create(data :: keyword) :: Connection.t()
  def create(data) do
    struct(Connection, data)
  end
end
