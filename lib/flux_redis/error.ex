defmodule FluxRedis.Error do
  @moduledoc """
  `FluxRedis` error module/struct.
  """
  @moduledoc since: "0.0.1"

  alias FluxRedis.Error
  alias FluxRedis.Error.Messages

  @typedoc "`t:FluxRedis.Error.t/0` message options."
  @doc since: "0.0.1"
  @type opts :: list({:format, :elixir | :python})

  @typedoc """
  `FluxRedis` error `t:struct/0`.

  Every `FluxRedis` error is a `t:FluxRedis.Error.t/0`.

  `Redix` errors are merged with `t:FluxRedis.Error.t/0` and can be accessed
  with `:detail` key inside `:metadata` `t:map/0`.
  """
  @typedoc since: "0.0.1"
  @type t :: %Error{
          reason: atom,
          metadata: map
        }

  defexception [:reason, :metadata]

  @doc false
  @spec build(atom, keyword) :: Error.t()
  def build(reason, metadata \\ []) do
    %Error{reason: reason, metadata: Map.new(metadata)}
  end

  @doc """
  Generate message `t:String.t/0` based on `:reason` and `:metadata`.

  ## Parameters

    - `error` - The `t:FluxRedis.Error.t/0`.

  ## Examples

      iex> error = %FluxRedis.Error{reason: :key_not_found, metadata: %{key: :id}}
      ...> FluxRedis.Error.message(error)
      "data does not have key 'id'"
  """
  @doc since: "0.0.1"
  @spec message(Error.t()) :: String.t()
  def message(%Error{metadata: metadata} = error) do
    fetch_metadata(message_raw(error), metadata)
  end

  @doc """
  Generate raw message `t:String.t/0` based on `:reason` to be used by external
  modules or services, such as `Gettext`.

  ## Parameters

    - `error` - The `t:FluxRedis.Error.t/0`.

    - `opts` - The `t:opts/0` `t:keyword/0`. Defaults to `[]`. Options are:

      - `:format` - Format message keys to a language standard. Options are:

        - `:elixir` - Message keys are defined as "%{key}", satisfying the
          `Gettext` pattern.

        - `:python` - Message keys are defined as "%(key)s", satisfying the
          [python gettext](https://docs.python.org/3/library/gettext.html)
          pattern.

  ## Examples

      iex> error = %FluxRedis.Error{reason: :key_not_found, metadata: %{key: :id}}
      ...> FluxRedis.Error.message_raw(error, format: :python)
      "data does not have key '%(key)s'"
  """
  @doc since: "0.0.1"
  @spec message_raw(Error.t(), opts) :: String.t()
  def message_raw(%Error{reason: reason}, opts \\ []) do
    Messages.get(reason, opts)
  end

  defp fetch_metadata(message_raw, metadata) do
    Enum.reduce(metadata, message_raw, &fetch_metadata_item/2)
  end

  defp fetch_metadata_item({key, value}, message) do
    String.replace(message, "%{#{key}}", fn _ -> to_string(value) end)
  end
end
