defmodule FluxRedis.HashManager do
  @moduledoc """
  Manage Redis Hash data as a JSON storage.

  After defining `hash_storages` on your `FluxRedis` application configuration,
  you can use the functions of this module to manage hashes and hash fields as
  JSON storage.

  A Redis hash maintain a JSON data in each field. Each hash field name is
  defined according to the keys defined by a hash in `hash_storages`.

  For more information about application configuration, check `FluxRedis` main
  module and [homepage](https://hexdocs.pm/flux_redis/).

  ## Command Options

    - `:no_reply?` - If `true`, the function returns `:ok` after successfully
      send the request to Redis. Disabled (`false`) in get-type functions and
      defaults to `true` in set-type functions. Accepts `t:boolean/0`.

    - `:timeout` - Milliseconds to wait for a request sending. Defaults to
      `5_000`. Accepts `t:integer/0` or `:infinity`.
  """
  @moduledoc since: "0.0.1"

  alias FluxRedis.{Error, Executer}
  alias FluxRedis.HashManager.Commands

  @type key_data_list :: list(list(key_data) | key_data)
  @type key_data :: atom | integer | float | String.t()

  @doc """
  Count the number of fields in a hash.

  Returns `{:ok, integer}` in case of success or `{:error, %FluxRedis.Error{}}`
  otherwise.

  This function always sets `:no_reply?` option to `false`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `opts` - `t:keyword/0` of options. Check `FluxRedis.HashManager` for more
    information.

  ## Examples

      iex> hash_storages = [users: :id]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> users = [%{id: 1, name: "John"}, %{id: 2, name: "Doe"}]
      ...> FluxRedis.HashManager.set_many(:users, users)
      ...> FluxRedis.HashManager.count(:users)
      {:ok, 2}
  """
  @doc since: "0.0.2"
  @spec count(atom, keyword) :: {:ok, integer} | {:error, Error.t()}
  def count(hash, opts \\ []) do
    {:ok, count!(hash, opts)}
  rescue
    error -> {:error, error}
  end

  @doc """
  Count the number of fields in a hash.

  Similar to `count/2`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [users: :id]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> users = [%{id: 1, name: "John"}, %{id: 2, name: "Doe"}]
      ...> FluxRedis.HashManager.set_many(:users, users)
      ...> FluxRedis.HashManager.count!(:users)
      2
  """
  @doc since: "0.0.2"
  @spec count!(atom, keyword) :: integer
  def count!(hash, opts \\ []) do
    hash
    |> Commands.count()
    |> Executer.execute_command!(force_reply(opts))
  end

  @doc """
  Removes a field from a hash.

  Returns `:ok` in case of success or `{:error, %FluxRedis.Error{}}` otherwise.

  By default, the option `no_reply?` is set to `true`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the values of each key defined in
    `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car)
      ...> FluxRedis.HashManager.delete(:cars, ["Honda", "Civic"])
      :ok
  """
  @doc since: "0.0.2"
  @spec delete(atom, list(key_data) | key_data, keyword) :: :ok | {:error, Error.t()}
  def delete(hash, key_data, opts \\ [no_reply?: true]) do
    delete!(hash, key_data, opts)
  rescue
    error -> {:error, error}
  end

  @doc """
  Removes a field from a hash.

  Similar to `delete/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car)
      ...> FluxRedis.HashManager.delete(:cars, ["Honda", "Civic"])
      :ok
  """
  @doc since: "0.0.2"
  @spec delete!(atom, list(key_data) | key_data, keyword) :: :ok
  def delete!(hash, key_data, opts \\ [no_reply?: true]) do
    hash
    |> Commands.delete(key_data)
    |> Executer.execute_command!(opts)
  end

  @doc """
  Removes one or more fields from a hash.

  Returns `:ok` in case of successful request and `no_reply?` is set to `true`.

  Returns `{:ok, amount_removed}` in case of success and `no_reply?` is set to
  `false`.

  Returns `{:error, %FluxRedis.Error{}}` on failure.

  By default, the option `no_reply?` is set to `true`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data_list` - The `t:key_data_list/0` with the values of each key
    defined in `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [animals: [:phylum, :family]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> animals = [
      ...>   %{family: "Canidae", phylum: "Chordata"},
      ...>   %{family: "Felidae", phylum: "Chordata"}
      ...> ]
      ...> FluxRedis.HashManager.set_many(:animals, animals)
      ...> animals_keys = [["Chordata", "Canidae"], ["Chordata", "Felidae"]]
      ...> alias FluxRedis.HashManager
      ...> HashManager.delete_many(:animals, animals_keys, no_reply?: false)
      {:ok, 2}
  """
  @doc since: "0.0.2"
  @spec delete_many(atom, key_data_list, keyword) :: :ok | {:ok, integer} | {:error, Error.t()}
  def delete_many(hash, key_data_list, opts \\ [no_reply?: true]) do
    hash
    |> delete_many!(key_data_list, opts)
    |> handle_result()
  rescue
    error -> {:error, error}
  end

  @doc """
  Removes one or more fields from a hash.

  Similar to `delete_many/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [animals: [:phylum, :family]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> animals = [
      ...>   %{family: "Canidae", phylum: "Chordata"},
      ...>   %{family: "Felidae", phylum: "Chordata"}
      ...> ]
      ...> FluxRedis.HashManager.set_many(:animals, animals)
      ...> animals_keys = [["Chordata", "Canidae"], ["Chordata", "Felidae"]]
      ...> alias FluxRedis.HashManager
      ...> HashManager.delete_many!(:animals, animals_keys, no_reply?: false)
      2
  """
  @doc since: "0.0.2"
  @spec delete_many!(atom, key_data_list, keyword) :: :ok | integer
  def delete_many!(hash, key_data_list, opts \\ [no_reply?: true]) do
    hash
    |> Commands.delete_many(key_data_list)
    |> Executer.execute_command!(opts)
  end

  @doc """
  Retrieves a field from a hash.

  Returns `{:ok, field}` in case of success or `{:error, %FluxRedis.Error{}}`
  otherwise.

  This function always sets `:no_reply?` option to `false`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the values of each key defined in
    `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car)
      ...> FluxRedis.HashManager.get(:cars, ["Honda", "Civic"])
      {:ok, %{brand: "Honda", model: "Civic", year: 2019}}
  """
  @doc since: "0.0.1"
  @spec get(atom, list(key_data) | key_data, keyword) :: {:ok, map} | {:error, Error.t()}
  def get(hash, key_data, opts \\ []) do
    {:ok, get!(hash, key_data, opts)}
  rescue
    error -> {:error, error}
  end

  @doc """
  Retrieves a field from a hash.

  Similar to `get/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car)
      ...> FluxRedis.HashManager.get!(:cars, ["Honda", "Civic"])
      %{brand: "Honda", model: "Civic", year: 2019}
  """
  @doc since: "0.0.2"
  @spec get!(atom, list(key_data) | key_data, keyword) :: map
  def get!(hash, key_data, opts \\ []) do
    hash
    |> Commands.get(key_data)
    |> Executer.execute_command!(force_reply(opts))
  end

  @doc """
  Retrieves all fields in a hash.

  Returns `{:ok, fields}` in case of success or `{:error, %FluxRedis.Error{}}`
  otherwise.

  This function always sets `:no_reply?` option to `false`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `opts` - `t:keyword/0` of options. Check `FluxRedis.HashManager` for more
    information.

  ## Examples

      iex> hash_storages = [users: :id]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> users = [%{id: 1, name: "John"}, %{id: 2, name: "Doe"}]
      ...> FluxRedis.HashManager.set_many(:users, users)
      ...> FluxRedis.HashManager.get_all(:users)
      {:ok, [%{id: 1, name: "John"}, %{id: 2, name: "Doe"}]}
  """
  @doc since: "0.0.1"
  @spec get_all(atom, keyword) :: {:ok, list(map)} | {:error, Error.t()}
  def get_all(hash, opts \\ []) do
    {:ok, get_all!(hash, opts)}
  rescue
    error -> {:error, error}
  end

  @doc """
  Retrieves all fields in a hash.

  Similar to `get_all/2`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [users: :id]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> users = [%{id: 1, name: "John"}, %{id: 2, name: "Doe"}]
      ...> FluxRedis.HashManager.set_many(:users, users)
      ...> FluxRedis.HashManager.get_all!(:users)
      [%{id: 1, name: "John"}, %{id: 2, name: "Doe"}]
  """
  @doc since: "0.0.2"
  @spec get_all!(atom, keyword) :: list(map)
  def get_all!(hash, opts \\ []) do
    hash
    |> Commands.get_all()
    |> Executer.execute_command!(force_reply(opts))
  end

  @doc """
  Retrieves one or more fields in a hash.

  Returns `{:ok, fields}` in case of success or `{:error, %FluxRedis.Error{}}`
  otherwise.

  This function always sets `:no_reply?` option to `false`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data_list` - The `t:key_data_list/0` with the values of each key
    defined in `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [animals: [:phylum, :family]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> animals = [
      ...>   %{family: "Canidae", phylum: "Chordata"},
      ...>   %{family: "Felidae", phylum: "Chordata"}
      ...> ]
      ...> FluxRedis.HashManager.set_many(:animals, animals)
      ...> animals_keys = [["Chordata", "Canidae"], ["Chordata", "Felidae"]]
      ...> FluxRedis.HashManager.get_many(:animals, animals_keys)
      {
        :ok,
        [
         %{family: "Canidae", phylum: "Chordata"},
         %{family: "Felidae", phylum: "Chordata"}
        ]
      }
  """
  @doc since: "0.0.1"
  @spec get_many(atom, key_data_list, keyword) :: {:ok, list(map)} | {:error, Error.t()}
  def get_many(hash, key_data_list, opts \\ []) do
    {:ok, get_many!(hash, key_data_list, opts)}
  rescue
    error -> {:error, error}
  end

  @doc """
  Retrieves one or more fields in a hash.

  Similar to `get_many/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [animals: [:phylum, :family]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> animals = [
      ...>   %{family: "Canidae", phylum: "Chordata"},
      ...>   %{family: "Felidae", phylum: "Chordata"}
      ...> ]
      ...> FluxRedis.HashManager.set_many(:animals, animals)
      ...> animals_keys = [["Chordata", "Canidae"], ["Chordata", "Felidae"]]
      ...> FluxRedis.HashManager.get_many!(:animals, animals_keys)
      [
        %{family: "Canidae", phylum: "Chordata"},
        %{family: "Felidae", phylum: "Chordata"}
      ]
  """
  @doc since: "0.0.2"
  @spec get_many!(atom, key_data_list, keyword) :: list(map)
  def get_many!(hash, key_data_list, opts \\ []) do
    hash
    |> Commands.get_many(key_data_list)
    |> Executer.execute_command!(force_reply(opts))
  end

  @doc """
  Retrieves one or more fields that match key values pattern in a hash.

  Returns `{:ok, fields}` in case of success or `{:error, %FluxRedis.Error{}}`
  otherwise.

  This function always sets `:no_reply?` option to `false`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the pattern of one or more keys
    defined in `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [animals: [:phylum, :family]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> animals = [
      ...>   %{family: "Canidae", phylum: "Chordata"},
      ...>   %{family: "Felidae", phylum: "Chordata"}
      ...> ]
      ...> FluxRedis.HashManager.set_many(:animals, animals)
      ...> pattern = ["*", "Can*"]
      ...> FluxRedis.HashManager.get_match(:animals, pattern)
      {:ok, [%{family: "Canidae", phylum: "Chordata"}]}
  """
  @doc since: "0.0.2"
  @spec get_match(atom, list(key_data) | key_data, keyword) ::
          {:ok, list(map)} | {:error, Error.t()}
  def get_match(hash, key_data, opts \\ []) do
    {:ok, get_match!(hash, key_data, opts)}
  rescue
    error -> {:error, error}
  end

  @doc """
  Retrieves one or more fields that match key values pattern in a hash.

  Similar to `get_match/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [animals: [:phylum, :family]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> animals = [
      ...>   %{family: "Canidae", phylum: "Chordata"},
      ...>   %{family: "Felidae", phylum: "Chordata"}
      ...> ]
      ...> FluxRedis.HashManager.set_many(:animals, animals)
      ...> pattern = ["*", "Can*"]
      ...> FluxRedis.HashManager.get_match!(:animals, pattern)
      [%{family: "Canidae", phylum: "Chordata"}]
  """
  @doc since: "0.0.2"
  @spec get_match!(atom, list(key_data) | key_data, keyword) :: list(map)
  def get_match!(hash, key_data, opts \\ []) do
    hash
    |> Commands.get_match(key_data)
    |> Executer.execute_command!(force_reply(opts))
  end

  @doc """
  Check if field exists in a hash.

  Returns `{:ok, boolean}` in case of success or `{:error, %FluxRedis.Error{}}`
  otherwise.

  This function always sets `:no_reply?` option to `false`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the values of each key defined in
    `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car)
      ...> FluxRedis.HashManager.has(:cars, ["Honda", "Civic"])
      {:ok, true}
      ...> FluxRedis.HashManager.has(:cars, ["Honda", "City"])
      {:ok, false}
  """
  @doc since: "0.0.2"
  @spec has(atom, list(key_data) | key_data, keyword) :: {:ok, boolean} | {:error, Error.t()}
  def has(hash, key_data, opts \\ []) do
    {:ok, has?(hash, key_data, opts)}
  rescue
    error -> {:error, error}
  end

  @doc """
  Check if field exists in a hash.

  Similar to `has/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car)
      ...> FluxRedis.HashManager.has?(:cars, ["Honda", "Civic"])
      true
      ...> FluxRedis.HashManager.has?(:cars, ["Honda", "City"])
      false
  """
  @doc since: "0.0.2"
  @spec has?(atom, list(key_data) | key_data, keyword) :: boolean
  def has?(hash, key_data, opts \\ []) do
    hash
    |> Commands.has(key_data)
    |> Executer.execute_command!(force_reply(opts))
  end

  @doc """
  Add or update a field in a hash.

  Returns `:ok` in case of successful request and `no_reply?` is set to `true`.

  Returns `{:ok, :set}` or `{:ok, :not_changed}` in case of success and
  `no_reply?` is set to `false`.

  Returns `{:error, %FluxRedis.Error{}}` on failure.

  By default, the option `no_reply?` is set to `true`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `data` - A `t:map/0`. Must have at least all keys defined in `FluxRedis`
    `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set(:cars, car, no_reply?: false)
      {:ok, :set}
      ...> FluxRedis.HashManager.set(:cars, car, no_reply?: false)
      {:ok, :not_changed}
  """
  @doc since: "0.0.1"
  @spec set(atom, map, keyword) :: :ok | {:ok, :set | :not_changed} | {:error, Error.t()}
  def set(hash, data, opts \\ [no_reply?: true]) do
    hash
    |> set!(data, opts)
    |> handle_result()
  rescue
    error -> {:error, error}
  end

  @doc """
  Add or update a field in a hash.

  Similar to `set/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set!(:cars, car, no_reply?: false)
      :set
      ...> FluxRedis.HashManager.set!(:cars, car, no_reply?: false)
      :not_changed
  """
  @doc since: "0.0.2"
  @spec set!(atom, map, keyword) :: :ok | :set | :not_changed
  def set!(hash, data, opts \\ [no_reply?: true]) do
    hash
    |> Commands.set!(data)
    |> Executer.execute_command!(opts)
  end

  @doc """
  Add or update one or more fields in a hash.

  Returns `:ok` in case of successful request and `no_reply?` is set to `true`.

  Returns `{:ok, amount_set}` in case of success and `no_reply?` is set to
  `false`.

  Returns `{:error, %FluxRedis.Error{}}` on failure.

  By default, the option `no_reply?` is set to `true`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `data_list` - A `t:list/1` of `t:map/0`. Each map Must have at least all
    keys defined in `FluxRedis` `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> civic = %{brand: "Honda", model: "Civic", year: 2019}
      ...> city = %{brand: "Honda", model: "City", year: 2018}
      ...> alias FluxRedis.HashManager
      ...> HashManager.set_many(:cars, [civic, city], no_reply?: false)
      {:ok, 2}
      ...> ram = %{brand: "Dodge", model: "Ram", year: 2020}
      ...> HashManager.set_many(:cars, [city, ram], no_reply?: false)
      {:ok, 1}
  """
  @doc since: "0.0.2"
  @spec set_many(atom, list(map), keyword) :: :ok | {:ok, integer} | {:error, Error.t()}
  def set_many(hash, data_list, opts \\ [no_reply?: true]) do
    hash
    |> set_many!(data_list, opts)
    |> handle_result()
  rescue
    error -> {:error, error}
  end

  @doc """
  Add or update one or more fields in a hash.

  Similar to `set_many/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> civic = %{brand: "Honda", model: "Civic", year: 2019}
      ...> city = %{brand: "Honda", model: "City", year: 2018}
      ...> alias FluxRedis.HashManager
      ...> HashManager.set_many!(:cars, [civic, city], no_reply?: false)
      ...> ram = %{brand: "Dodge", model: "Ram", year: 2020}
      ...> HashManager.set_many!(:cars, [city, ram], no_reply?: false)
      1
  """
  @doc since: "0.0.2"
  @spec set_many!(atom, list(map), keyword) :: :ok | integer
  def set_many!(hash, data_list, opts \\ [no_reply?: true]) do
    hash
    |> Commands.set_many!(data_list)
    |> Executer.execute_command!(opts)
  end

  @doc """
  Add a field in a hash.

  This function does not update a field, use `set/3` to update a field.

  Returns `:ok` in case of successful request and `no_reply?` is set to `true`.

  Returns `{:ok, :set}` or `{:ok, :not_changed}` in case of success and
  `no_reply?` is set to `false`.

  Returns `{:error, %FluxRedis.Error{}}` on failure.

  By default, the option `no_reply?` is set to `true`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `data` - A `t:map/0`. Must have at least all keys defined in `FluxRedis`
    `hash_storages` application configuration.

  - `opts` - `t:keyword/0` with command options. Check `FluxRedis.HashManager`
    for more information.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set_new(:cars, car, no_reply?: false)
      {:ok, :set}
      ...> FluxRedis.HashManager.set_new(:cars, car, no_reply?: false)
      {:ok, :not_changed}
  """
  @doc since: "0.0.2"
  @spec set_new(atom, map, keyword) :: :ok | {:ok, :set | :not_changed} | {:error, Error.t()}
  def set_new(hash, data, opts \\ [no_reply?: true]) do
    hash
    |> set_new!(data, opts)
    |> handle_result()
  rescue
    error -> {:error, error}
  end

  @doc """
  Add a field in a hash.

  Similar to `set_new/3`, but raises `t:FluxRedis.Error.t/0` on failure.

  ## Examples

      iex> hash_storages = [cars: [:brand, :model]]
      ...> Application.put_env(:flux_redis, :hash_storages, hash_storages)
      ...> car = %{brand: "Honda", model: "Civic", year: 2019}
      ...> FluxRedis.HashManager.set_new!(:cars, car, no_reply?: false)
      :set
      ...> FluxRedis.HashManager.set_new!(:cars, car, no_reply?: false)
      :not_changed
  """
  @doc since: "0.0.2"
  @spec set_new!(atom, map, keyword) :: :ok | :set | :not_changed
  def set_new!(hash, data, opts \\ [no_reply?: true]) do
    hash
    |> Commands.set_new!(data)
    |> Executer.execute_command!(opts)
  end

  defp force_reply(opts), do: Keyword.put(opts, :no_reply?, false)
  defp handle_result(:ok), do: :ok
  defp handle_result(result), do: {:ok, result}
end
