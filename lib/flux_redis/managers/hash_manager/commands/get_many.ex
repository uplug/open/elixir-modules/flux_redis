defmodule FluxRedis.HashManager.Commands.GetMany do
  @moduledoc """
  Command and parser generator to retrieve one or more fields from a hash.
  """
  @moduledoc since: "0.0.1"

  alias FluxRedis.HashManager.Parser

  @type key_data_list :: list(list(key_data) | key_data)
  @type key_data :: atom | integer | float | String.t()

  @type command :: list
  @type parser :: (list(String.t()) -> list(map))

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to retrieve one or more
  fields from a hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data_list` - The `t:key_data_list/0` with the values of each key
    defined in `FluxRedis` `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.get_many/3`.
  """
  @moduledoc since: "0.0.1"
  @spec get_many(atom, key_data_list) :: {command, parser}
  def get_many(hash, key_data_list) do
    {build_command(hash, key_data_list), &parse_result!/1}
  end

  defp build_command(hash, key_data_list) do
    ["HMGET", hash] ++ Parser.join_key_data_list(key_data_list)
  end

  defp parse_result!(payload) do
    payload
    |> Enum.reduce([], &parse_item!/2)
    |> Enum.reverse()
  end

  defp parse_item!(item, data) do
    if item == nil do
      data
    else
      [Parser.decode_json!(item)] ++ data
    end
  end
end
