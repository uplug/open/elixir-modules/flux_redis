defmodule FluxRedis.HashManager.Commands.Get do
  @moduledoc """
  Command and parser generator to retrieve a field in a hash.
  """
  @moduledoc since: "0.0.1"

  alias FluxRedis.Error
  alias FluxRedis.HashManager.Parser

  @type key_data :: atom | integer | float | String.t()

  @type command :: list
  @type parser :: (String.t() -> map)

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to retrieve a field in a
  hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the values of each key defined in
    `FluxRedis` `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.get/3`.
  """
  @moduledoc since: "0.0.1"
  @spec get(atom, list(key_data) | key_data) :: {command, parser}
  def get(hash, key_data), do: {build_command(hash, key_data), &parse_result!/1}
  defp build_command(hash, key_data), do: ["HGET", hash, Parser.join_key_data(key_data)]
  defp parse_result!(nil), do: raise(Error.build(:not_found))
  defp parse_result!(payload), do: Parser.decode_json!(payload)
end
