defmodule FluxRedis.HashManager.Commands.GetMatch do
  @moduledoc """
  Command and parser generator to retrieve one or more fields that match key
  values pattern in a hash.
  """
  @moduledoc since: "0.0.2"

  alias FluxRedis.HashManager.Parser

  @type key_data :: atom | integer | float | String.t()

  @type command :: list
  @type parser :: (list(String.t()) -> list(map))

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to retrieve one or more
  fields that match key values pattern in a hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the pattern of one or more keys
    defined in `FluxRedis` `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.get_match/3`.
  """
  @moduledoc since: "0.0.2"
  @spec get_match(atom, list(key_data) | key_data) :: {command, parser}
  def get_match(hash, key_data) do
    {build_command(hash, key_data), &parse_result!/1}
  end

  defp build_command(hash, key_data) do
    ["HSCAN", hash, 0, "MATCH", Parser.join_key_data(key_data), "COUNT", 100_000]
  end

  defp parse_result!([_cursor, payload]) do
    payload
    |> Enum.drop_every(2)
    |> Enum.map(&Parser.decode_json!/1)
  end
end
