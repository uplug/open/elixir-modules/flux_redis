defmodule FluxRedis.HashManager.Commands.DeleteMany do
  @moduledoc """
  Command and parser generator to remove one or more fields from a hash.
  """
  @moduledoc since: "0.0.2"

  alias FluxRedis.HashManager.Parser

  @type key_data_list :: list(list(key_data) | key_data)
  @type key_data :: atom | integer | float | String.t()

  @type command :: list
  @type parser :: (integer -> integer)

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to remove one or more fields
  from a hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data_list` - The `t:key_data_list/0` with the values of each key
    defined in `FluxRedis` `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.delete_many/3`.
  """
  @doc since: "0.0.2"
  @spec delete_many(atom, key_data_list) :: {command, parser}
  def delete_many(hash, key_data_list) do
    {build_command(hash, key_data_list), &parse_result/1}
  end

  defp build_command(hash, key_data_list) do
    ["HDEL", hash] ++ Parser.join_key_data_list(key_data_list)
  end

  defp parse_result(amount_removed), do: amount_removed
end
