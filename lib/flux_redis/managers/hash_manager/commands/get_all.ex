defmodule FluxRedis.HashManager.Commands.GetAll do
  @moduledoc """
  Command and parser generator to remove one or more fields from a hash.
  """
  @moduledoc since: "0.0.1"

  alias FluxRedis.HashManager.Parser

  @type command :: list
  @type parser :: (list(String.t()) -> list(map))

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to retrieves all fields in a
  hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.get_all/2`.
  """
  @doc since: "0.0.1"
  @spec get_all(atom) :: {command, parser}
  def get_all(hash), do: {build_command(hash), &parse_result!/1}
  defp build_command(hash), do: ["HVALS", hash]
  defp parse_result!(payload), do: Enum.map(payload, &Parser.decode_json!/1)
end
