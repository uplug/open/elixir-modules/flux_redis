defmodule FluxRedis.HashManager.Commands.Count do
  @moduledoc """
  Command and parser generator to count the number of fields in a hash.
  """
  @moduledoc since: "0.0.2"

  @type command :: list
  @type parser :: (integer -> integer)

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to count the number of
  fields in a hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.count/2`.
  """
  @doc since: "0.0.2"
  @spec count(atom) :: {command, parser}
  def count(hash), do: {build_command(hash), &parse_result/1}
  defp build_command(hash), do: ["HLEN", hash]
  defp parse_result(size), do: size
end
