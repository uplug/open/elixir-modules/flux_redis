defmodule FluxRedis.HashManager.Commands.SetNew do
  @moduledoc """
  Command and parser generator to add a new field in a hash.
  """
  @moduledoc since: "0.0.2"

  alias FluxRedis.HashManager.Parser

  @type command :: list
  @type parser :: (integer -> :set | :not_changed)

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to add a new field in a
  hash.

  Raises `t:FluxRedis.Error.t/0` if failed to generate `t:command/0`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `data` - A `t:map/0`. Must have at least all keys defined in `FluxRedis`
    `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.set_new/3`.
  """
  @moduledoc since: "0.0.2"
  @spec set_new!(atom, map) :: {command, parser}
  def set_new!(hash, data) do
    {key, json} = Parser.prepare_data!(hash, data)
    do_set_new(hash, key, json)
  end

  defp do_set_new(hash, key, json) do
    {["HSETNX", hash, key, json], &parse_result/1}
  end

  defp parse_result(1), do: :set
  defp parse_result(_payload), do: :not_changed
end
