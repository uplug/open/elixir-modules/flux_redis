defmodule FluxRedis.HashManager.Commands.Has do
  @moduledoc """
  Command and parser generator to check if field exists in a hash.
  """
  @moduledoc since: "0.0.2"

  alias FluxRedis.HashManager.Parser

  @type key_data :: atom | integer | float | String.t()

  @type command :: list
  @type parser :: (integer -> boolean)

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to check if field exists in
  a hash.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `key_data` - The `t:key_data/0` with the values of each key defined in
    `FluxRedis` `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.has/3`.
  """
  @doc since: "0.0.2"
  @spec has(atom, list(key_data) | key_data) :: {command, parser}
  def has(hash, key_data), do: {build_command(hash, key_data), &parse_result/1}
  defp build_command(hash, key_data), do: ["HEXISTS", hash, Parser.join_key_data(key_data)]
  defp parse_result(has), do: has == 1
end
