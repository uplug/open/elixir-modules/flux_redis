defmodule FluxRedis.HashManager.Commands.SetMany do
  @moduledoc """
  Command and parser generator to add or update one or more fields in a hash.
  """
  @moduledoc since: "0.0.2"

  alias FluxRedis.HashManager.Parser

  @type command :: list
  @type parser :: (integer -> integer)

  @doc """
  Generates the `t:command/0` and the `t:parser/0` to add or update one or more
  fields in a hash.

  Raises `t:FluxRedis.Error.t/0` if failed to generate `t:command/0`.

  ## Parameters

  - `hash` - The `t:atom/0` defined in `FluxRedis` `hash_storages` application
    configuration.

  - `data_list` - A `t:list/1` of `t:map/0`. Each map Must have at least all
    keys defined in `FluxRedis` `hash_storages` application configuration.

  ## Examples

  For practical examples, please check `FluxRedis.HashManager.set_many/3`.
  """
  @moduledoc since: "0.0.2"
  @spec set_many!(atom, list(map)) :: {command, parser}
  def set_many!(hash, data_list) do
    do_set_many(hash, prepare_data_list!(hash, data_list))
  end

  defp do_set_many(hash, prepared_data_list) do
    {build_command(hash, prepared_data_list), &parse_result/1}
  end

  defp build_command(hash, prepared_data_list) do
    ["HSET", hash] ++ prepared_data_list
  end

  defp prepare_data_list!(hash, data_list) do
    data_list
    |> Enum.reduce([], &prepare_data!(hash, &1, &2))
    |> Enum.reverse()
  end

  defp prepare_data!(hash, data, prepared_data_list) do
    {key, json} = Parser.prepare_data!(hash, data)
    [json, key] ++ prepared_data_list
  end

  defp parse_result(amount_set), do: amount_set
end
