defmodule FluxRedis.HashManager.Parser do
  @moduledoc false

  alias FluxRedis.{Config, Error, Parser}

  @type key_data :: atom | integer | float | String.t()

  defdelegate decode_json!(payload), to: Parser
  defdelegate encode_to_json!(data), to: Parser

  @spec extract_key_data!(atom, map) :: list(key_data)
  def extract_key_data!(hash, data) do
    hash
    |> Config.hash_keys()
    |> Enum.map(&extract_key!(data, &1))
  end

  @spec join_key_data(list(key_data) | key_data) :: String.t()
  def join_key_data(key_data) do
    if is_list(key_data) do
      Enum.join(key_data, "|")
    else
      key_data
    end
  end

  @spec join_key_data_list(list(list(key_data) | key_data)) :: list(String.t())
  def join_key_data_list(key_data_list) do
    Enum.map(key_data_list, &join_key_data/1)
  end

  @spec prepare_data!(atom, map) :: {String.t(), String.t()}
  def prepare_data!(hash, data) do
    {create_key!(hash, data), encode_to_json!(data)}
  end

  defp create_key!(hash, data) do
    hash
    |> extract_key_data!(data)
    |> join_key_data()
  end

  defp extract_key!(data, key) do
    Map.get_lazy(data, key, fn -> raise Error.build(:key_not_found, key: key) end)
  end
end
