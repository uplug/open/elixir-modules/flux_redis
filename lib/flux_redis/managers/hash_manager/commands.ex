defmodule FluxRedis.HashManager.Commands do
  @moduledoc """
  Provides the commands to be used with a `FluxRedis.PipelineManager` function.

  These commands is also used by the `FluxRedis.HashManager` to generate the
  command to be executed.
  """
  @moduledoc since: "0.0.2"

  alias FluxRedis.HashManager.Commands.{
    Count,
    Delete,
    DeleteMany,
    Get,
    GetAll,
    GetMany,
    GetMatch,
    Has,
    Set,
    SetMany,
    SetNew
  }

  defdelegate count(hash), to: Count
  defdelegate delete(hash, key_data), to: Delete
  defdelegate delete_many(hash, key_data_list), to: DeleteMany
  defdelegate get(hash, key_data), to: Get
  defdelegate get_all(hash), to: GetAll
  defdelegate get_many(hash, key_data_list), to: GetMany
  defdelegate get_match(hash, key_data), to: GetMatch
  defdelegate has(hash, key_data), to: Has
  defdelegate set!(hash, data), to: Set
  defdelegate set_many!(hash, data_list), to: SetMany
  defdelegate set_new!(hash, data), to: SetNew
end
