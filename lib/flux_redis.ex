defmodule FluxRedis do
  @moduledoc """
  Integrate Redis to Elixir projects.
  """
  @moduledoc since: "0.0.1"

  alias FluxRedis.Config

  @typedoc """
  A `t:String.t/0` with the pattern defined according to `Redix.start_link/1`
  URI format.

  Check [Flux Redis](https://hexdocs.pm/flux_redis) for detailed information
  about application configuration.
  """
  @typedoc since: "0.0.1"
  @type redis_uri :: String.t()

  @typedoc """
  A `t:keyword/0` set according to `Redix.start_link/1`. It is set by
  `:flux_redis` application configuration data.

  Check [Flux Redis](https://hexdocs.pm/flux_redis) for detailed information
  about application configuration.
  """
  @typedoc since: "0.0.1"
  @type specs :: [
          backoff_initial: integer,
          backoff_max: integer,
          exit_on_disconnection: boolean,
          name: atom,
          sync_connect: boolean,
          timeout: integer
        ]

  @doc """
  A child spec set according to `:flux_redis` application configuration.

  To use it, simply call this function as a child of your application or in one
  of the supervisors. For example:

  ```elixir
  defmodule MyApp.Application do
    use Application

    @impl Application
    def start(_type, _args) do
      import Supervisor.Spec

      children = [
        FluxRedis.child_spec()
      ]

      opts = [strategy: :one_for_one]

      Supervisor.start_link(children, opts)
    end
  end
  ```
  """
  @doc since: "0.0.1"
  @spec child_spec :: {Redix, {redis_uri, specs}}
  def child_spec do
    connection = Config.connection()

    opts = [
      backoff_initial: connection.backoff_initial,
      backoff_max: connection.backoff_max,
      exit_on_disconnection: connection.exit_on_disconnection,
      name: connection.name,
      sync_connect: connection.sync_connect,
      timeout: connection.timeout
    ]

    {Redix, {connection.uri, opts}}
  end
end
