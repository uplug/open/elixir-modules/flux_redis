import Config

config :flux_redis,
  connection: [
    uri: "redis://redis",
    timeout: 5_000,
    sync_connect: true,
    exit_on_disconnection: false,
    backoff_initial: 1_000,
    backoff_max: 10_000
  ],
  hash_storages: []
