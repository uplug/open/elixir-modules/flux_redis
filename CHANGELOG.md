# Changelog

## 0.0.2 - 2019-11-17

- **Added**:

  - Documentation for all useful public modules and functions.

  - The following functions at `FluxRedis.HashManager`:

    - `FluxRedis.HashManager.count/2`
    - `FluxRedis.HashManager.count!/2`
    - `FluxRedis.HashManager.delete/3`
    - `FluxRedis.HashManager.delete!/3`
    - `FluxRedis.HashManager.delete_many/3`
    - `FluxRedis.HashManager.delete_many!/3`
    - `FluxRedis.HashManager.get!/3`
    - `FluxRedis.HashManager.get_all!/2`
    - `FluxRedis.HashManager.get_many!/3`
    - `FluxRedis.HashManager.get_match/3`
    - `FluxRedis.HashManager.get_match!/3`
    - `FluxRedis.HashManager.has/3`
    - `FluxRedis.HashManager.has?/3`
    - `FluxRedis.HashManager.set!/3`
    - `FluxRedis.HashManager.set_many/3`
    - `FluxRedis.HashManager.set_many!/3`
    - `FluxRedis.HashManager.set_new/3`
    - `FluxRedis.HashManager.set_new!/3`

  - The following modules, children of `FluxRedis.HashManager.Commands`:

    - `FluxRedis.HashManager.Commands.Count`
    - `FluxRedis.HashManager.Commands.Delete`
    - `FluxRedis.HashManager.Commands.DeleteMany`
    - `FluxRedis.HashManager.Commands.GetMatch`
    - `FluxRedis.HashManager.Commands.Has`
    - `FluxRedis.HashManager.Commands.SetMany`
    - `FluxRedis.HashManager.Commands.SetNew`

  - The module `FluxRedis.PipelineManager`.

- **Changed**:

  - Updated the internal structure for commands, without changing the API
    output.

## 0.0.1 - 2019-11-12

- **Added**:

  - The project is available on GitLab and Hex.

- **Notes**:

  - Minor changes (0.0.x) from the current version will be logged to this file.

  - When a major change is released (0.x.0 or x.0.0), the changelog of the
    previous major change will be grouped as a single change.
