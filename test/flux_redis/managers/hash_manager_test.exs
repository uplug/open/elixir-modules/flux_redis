defmodule FluxRedis.HashManagerTest do
  use ExUnit.Case

  alias FluxRedis.{Error, HashManager}
  alias FluxRedisTest.EnvManager
  alias FluxRedisTest.Factories.{Car, User}
  alias FluxRedisTest.RedisManager

  doctest HashManager

  setup_all do
    RedisManager.connect()
  end

  setup do
    on_exit(fn ->
      RedisManager.reset()
      EnvManager.reset()
    end)
  end

  describe "HashManager.count!/2" do
    test "succeeds if empty storage" do
      assert HashManager.count!(:users) == 0
    end

    test "succeeds if non-empty storage" do
      User.create(10)
      assert HashManager.count!(:users) == 10
    end

    test "fails if request timed out" do
      error = assert_raise Error, fn -> HashManager.count!(:users, timeout: 0) end
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.count/2" do
    test "succeeds if empty storage" do
      assert HashManager.count(:users) == {:ok, 0}
    end

    test "succeeds if non-empty storage" do
      User.create(10)
      assert HashManager.count(:users) == {:ok, 10}
    end

    test "fails if request timed out" do
      assert {:error, %Error{} = error} = HashManager.count(:users, timeout: 0)
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.delete!/3" do
    test "succeeds if element does not exists o Redis" do
      assert HashManager.delete!(:users, 1) == :ok
    end

    test "succeeds if element exists on Redis" do
      User.create()
      assert HashManager.delete!(:users, 1) == :ok
    end

    test "fails if request timed out" do
      error = assert_raise Error, fn -> HashManager.delete!(:users, 1, timeout: 0) end
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.delete/3" do
    test "succeeds if element does not exists o Redis" do
      assert HashManager.delete(:users, 1) == :ok
    end

    test "succeeds if element exists on Redis" do
      User.create()
      assert HashManager.delete(:users, 1) == :ok
    end

    test "fails if request timed out" do
      assert {:error, %Error{} = error} = HashManager.delete(:users, 1, timeout: 0)
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.delete_many!/3" do
    test "succeeds if empty storage" do
      assert HashManager.delete_many!(:users, [1, 2, 3]) == :ok
    end

    test "succeeds if non-empty storage" do
      User.create(10)
      assert HashManager.delete_many!(:users, [1, 2, 3], no_reply?: false) == 3
    end

    test "fails if request timed out" do
      error =
        assert_raise Error, fn -> HashManager.delete_many!(:users, [1, 2, 3], timeout: 0) end

      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.delete_many/3" do
    test "succeeds if empty storage" do
      assert HashManager.delete_many(:users, [1, 2, 3], no_reply?: false) == {:ok, 0}
    end

    test "succeeds if non-empty storage" do
      User.create(10)
      assert HashManager.delete_many(:users, [1, 2, 3]) == :ok
    end

    test "fails if request timed out" do
      assert {:error, %Error{} = error} = HashManager.delete_many(:users, [1, 2, 3], timeout: 0)
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.get!/3" do
    test "succeeds if element exists on Redis" do
      user = User.create()
      assert HashManager.get!(:users, 1) == user
    end

    test "fails if data on Redis is not JSON" do
      RedisManager.add_hash_element(:users, 1, "i am not json")
      error = assert_raise Error, fn -> HashManager.get!(:users, 1) end
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end

    test "fails if element does not exists on Redis" do
      error = assert_raise Error, fn -> HashManager.get!(:unknown, "unknown") end
      assert error.reason == :not_found
      assert Error.message(error) == "data not found"
    end

    test "fails if request timed out" do
      error = assert_raise Error, fn -> HashManager.get!(:users, 1, timeout: 0) end
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.get/3" do
    test "succeeds if element exists on Redis" do
      user = User.create()
      assert HashManager.get(:users, 1) == {:ok, user}
    end

    test "fails if data on Redis is not JSON" do
      RedisManager.add_hash_element(:users, 1, "i am not json")
      assert {:error, %Error{} = error} = HashManager.get(:users, 1)
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end

    test "fails if element does not exists on Redis" do
      assert {:error, %Error{} = error} = HashManager.get(:unknown, "unknown")
      assert error.reason == :not_found
      assert Error.message(error) == "data not found"
    end

    test "fails if request timed out" do
      assert {:error, %Error{} = error} = HashManager.get(:users, 1, timeout: 0)
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.get_all!/2" do
    test "succeeds if empty storage" do
      assert HashManager.get_all!(:users) == []
    end

    test "succeeds if non-empty storage" do
      users = User.create(10)
      assert HashManager.get_all!(:users) == users
    end

    test "fails if data on Redis storage is not JSON" do
      RedisManager.add_hash_element(:users, 1, "i am not json")
      error = assert_raise Error, fn -> HashManager.get_all!(:users) end
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end
  end

  describe "HashManager.get_all/2" do
    test "succeeds if empty storage" do
      assert HashManager.get_all(:users) == {:ok, []}
    end

    test "succeeds if non-empty storage" do
      users = User.create(10)
      assert HashManager.get_all(:users) == {:ok, users}
    end

    test "fails if data on Redis storage is not JSON" do
      RedisManager.add_hash_element(:users, 1, "i am not json")
      assert {:error, %Error{} = error} = HashManager.get_all(:users)
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end
  end

  describe "HashManager.get_many!/3" do
    test "succeeds if empty storage" do
      assert HashManager.get_many!(:users, [1, 2, 3]) == []
    end

    test "succeeds if non-empty storage" do
      [user1, user2 | _users] = User.create(10)
      assert HashManager.get_many!(:users, [1, 2]) == [user1, user2]
    end

    test "fails if data on Redis storage is not JSON" do
      User.create(10)
      RedisManager.add_hash_element(:users, 15, "i am not json")
      error = assert_raise Error, fn -> HashManager.get_many!(:users, [5, 15]) end
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end
  end

  describe "HashManager.get_many/3" do
    test "succeeds if empty storage" do
      assert HashManager.get_many(:users, [1, 2, 3]) == {:ok, []}
    end

    test "succeeds if non-empty storage" do
      [user1, user2 | _users] = User.create(10)
      assert HashManager.get_many(:users, [1, 2]) == {:ok, [user1, user2]}
    end

    test "fails if data on Redis storage is not JSON" do
      User.create(10)
      RedisManager.add_hash_element(:users, 15, "i am not json")
      assert {:error, %Error{} = error} = HashManager.get_many(:users, [5, 15])
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end
  end

  describe "HashManager.get_match/3" do
    test "succeeds if empty storage" do
      assert HashManager.get_match(:cars, ["F*", "C*"]) == {:ok, []}
    end

    test "succeeds if non-empty storage" do
      Car.create(300)
      assert {:ok, users} = HashManager.get_match(:cars, ["F*", "C*"])
      assert length(users) == 4
    end

    test "succeeds if non-empty storage, with match \"*\"" do
      User.create(10)
      assert {:ok, users} = HashManager.get_match(:users, "*")
      assert length(users) == 10
    end

    test "fails if data on Redis storage is not JSON" do
      Car.create(10)
      RedisManager.add_hash_element(:cars, "Fiat|City", "i am not json")
      assert {:error, %Error{} = error} = HashManager.get_match(:cars, ["F*", "C*"])
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end
  end

  describe "HashManager.get_match!/3" do
    test "succeeds if empty storage" do
      assert HashManager.get_match!(:cars, ["F*", "C*"]) == []
    end

    test "succeeds if non-empty storage" do
      Car.create(300)
      assert users = HashManager.get_match!(:cars, ["F*", "C*"])
      assert length(users) == 4
    end

    test "succeeds if non-empty storage, with match \"*\"" do
      User.create(10)
      assert users = HashManager.get_match!(:users, "*")
      assert length(users) == 10
    end

    test "fails if data on Redis storage is not JSON" do
      Car.create(10)
      RedisManager.add_hash_element(:cars, "Fiat|City", "i am not json")
      error = assert_raise Error, fn -> HashManager.get_match!(:cars, ["F*", "C*"]) end
      assert error.reason == :decode_failed
      assert Error.message(error) == "payload is not JSON"
    end
  end

  describe "HashManager.has?/3" do
    test "succeeds if not found" do
      refute HashManager.has?(:users, 1)
    end

    test "succeeds if found" do
      User.create()
      assert HashManager.has?(:users, 1)
    end

    test "fails if request timed out" do
      error = assert_raise Error, fn -> HashManager.has?(:users, 1, timeout: 0) end
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.has/3" do
    test "succeeds if not found" do
      assert HashManager.has(:users, 1) == {:ok, false}
    end

    test "succeeds if found" do
      User.create()
      assert HashManager.has(:users, 1) == {:ok, true}
    end

    test "fails if request timed out" do
      assert {:error, %Error{} = error} = HashManager.has(:users, 1, timeout: 0)
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "HashManager.set!/3" do
    test "succeeds if valid data" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set!(:cars, car) == :ok
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply)" do
      Car.create(0)
      car = Car.get(1)

      assert HashManager.set!(:cars, car, no_reply?: false) == :set
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply, not changed)" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set!(:cars, car, no_reply?: false) == :set
      assert HashManager.set!(:cars, car, no_reply?: false) == :not_changed
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "raise if key data does not exist" do
      Car.create(0)
      invalid_car = Map.delete(Car.get(1), :brand)
      error = assert_raise Error, fn -> HashManager.set!(:cars, invalid_car) end
      assert error.reason == :key_not_found
      assert Error.message(error) == "data does not have key 'brand'"
      assert Error.message_raw(error, format: :python) == "data does not have key '%(key)s'"
    end

    test "fails if data cannot be encoded to JSON" do
      Car.create(0)
      invalid_car = Map.put(Car.get(1), :i_will, {:create, :error})
      error = assert_raise Error, fn -> HashManager.set!(:cars, invalid_car) end
      assert error.reason == :encode_failed
      assert Error.message(error) == "data cannot be converted to JSON"
    end
  end

  describe "HashManager.set/3" do
    test "succeeds if valid data" do
      Car.create()
      car = Car.get(1)
      assert HashManager.set(:cars, car) == :ok
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply)" do
      Car.create(0)
      car = Car.get(1)

      assert HashManager.set(:cars, car, no_reply?: false) == {:ok, :set}
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply, not changed)" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set(:cars, car, no_reply?: false) == {:ok, :set}
      assert HashManager.set(:cars, car, no_reply?: false) == {:ok, :not_changed}
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "fails if key data does not exist" do
      Car.create(0)
      invalid_car = Map.delete(Car.get(1), :brand)
      assert {:error, %Error{} = error} = HashManager.set(:cars, invalid_car)
      assert error.reason == :key_not_found
      assert Error.message(error) == "data does not have key 'brand'"
      assert Error.message_raw(error, format: :python) == "data does not have key '%(key)s'"
    end

    test "fails if data cannot be encoded to JSON" do
      Car.create(0)
      invalid_car = Map.put(Car.get(1), :i_will, {:create, :error})
      assert {:error, %Error{} = error} = HashManager.set(:cars, invalid_car)
      assert error.reason == :encode_failed
      assert Error.message(error) == "data cannot be converted to JSON"
    end
  end

  describe "HashManager.set_many!/3" do
    test "succeeds if valid data" do
      Car.create(0)
      cars = Car.get_many(10)
      assert HashManager.set_many!(:cars, cars) == :ok
      assert HashManager.count(:cars) == {:ok, 10}
    end

    test "succeeds if valid data (with reply)" do
      Car.create(0)
      cars = Car.get_many(10)

      assert HashManager.set_many!(:cars, cars, no_reply?: false) == 10
      assert HashManager.count(:cars) == {:ok, 10}
    end

    test "succeeds if valid data (with reply, not changed)" do
      Car.create(0)
      cars = Car.get_many(10)
      assert HashManager.set_many!(:cars, cars, no_reply?: false) == 10
      assert HashManager.set_many!(:cars, cars, no_reply?: false) == 0
      assert HashManager.count(:cars) == {:ok, 10}
    end

    test "raise if key data does not exist" do
      Car.create(0)
      invalid_cars = Enum.map(Car.get_many(10), &Map.delete(&1, :brand))
      error = assert_raise Error, fn -> HashManager.set_many!(:cars, invalid_cars) end
      assert error.reason == :key_not_found
      assert Error.message(error) == "data does not have key 'brand'"
      assert Error.message_raw(error, format: :python) == "data does not have key '%(key)s'"
    end

    test "fails if data cannot be encoded to JSON" do
      Car.create(0)
      invalid_cars = Enum.map(Car.get_many(10), &Map.put(&1, :i_will, {:create, :error}))
      error = assert_raise Error, fn -> HashManager.set_many!(:cars, invalid_cars) end
      assert error.reason == :encode_failed
      assert Error.message(error) == "data cannot be converted to JSON"
    end
  end

  describe "HashManager.set_many/3" do
    test "succeeds if valid data" do
      Car.create(0)
      cars = Car.get_many(10)
      assert HashManager.set_many(:cars, cars) == :ok
      assert HashManager.count(:cars) == {:ok, 10}
    end

    test "succeeds if valid data (with reply)" do
      Car.create(0)
      cars = Car.get_many(10)

      assert HashManager.set_many(:cars, cars, no_reply?: false) == {:ok, 10}
      assert HashManager.count(:cars) == {:ok, 10}
    end

    test "succeeds if valid data (with reply, not changed)" do
      Car.create(0)
      cars = Car.get_many(10)
      assert HashManager.set_many(:cars, cars, no_reply?: false) == {:ok, 10}
      assert HashManager.set_many(:cars, cars, no_reply?: false) == {:ok, 0}
      assert HashManager.count(:cars) == {:ok, 10}
    end

    test "raise if key data does not exist" do
      Car.create(0)
      invalid_cars = Enum.map(Car.get_many(10), &Map.delete(&1, :brand))
      assert {:error, %Error{} = error} = HashManager.set_many(:cars, invalid_cars)
      assert error.reason == :key_not_found
      assert Error.message(error) == "data does not have key 'brand'"
      assert Error.message_raw(error, format: :python) == "data does not have key '%(key)s'"
    end

    test "fails if data cannot be encoded to JSON" do
      Car.create(0)
      invalid_cars = Enum.map(Car.get_many(10), &Map.put(&1, :i_will, {:create, :error}))
      assert {:error, %Error{} = error} = HashManager.set_many(:cars, invalid_cars)
      assert error.reason == :encode_failed
      assert Error.message(error) == "data cannot be converted to JSON"
    end
  end

  describe "HashManager.set_new!/3" do
    test "succeeds if valid data" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set_new!(:cars, car) == :ok
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply)" do
      Car.create(0)
      car = Car.get(1)

      assert HashManager.set_new!(:cars, car, no_reply?: false) == :set
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply, not changed)" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set_new!(:cars, car, no_reply?: false) == :set
      assert HashManager.set_new!(:cars, car, no_reply?: false) == :not_changed
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "raise if key data does not exist" do
      Car.create(0)
      invalid_car = Map.delete(Car.get(1), :brand)
      error = assert_raise Error, fn -> HashManager.set_new!(:cars, invalid_car) end
      assert error.reason == :key_not_found
      assert Error.message(error) == "data does not have key 'brand'"
      assert Error.message_raw(error, format: :python) == "data does not have key '%(key)s'"
    end

    test "fails if data cannot be encoded to JSON" do
      Car.create(0)
      invalid_car = Map.put(Car.get(1), :i_will, {:create, :error})
      error = assert_raise Error, fn -> HashManager.set_new!(:cars, invalid_car) end
      assert error.reason == :encode_failed
      assert Error.message(error) == "data cannot be converted to JSON"
    end
  end

  describe "HashManager.set_new/3" do
    test "succeeds if valid data" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set_new(:cars, car) == :ok
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply)" do
      Car.create(0)
      car = Car.get(1)

      assert HashManager.set_new(:cars, car, no_reply?: false) == {:ok, :set}
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "succeeds if valid data (with reply, not changed)" do
      Car.create(0)
      car = Car.get(1)
      assert HashManager.set_new(:cars, car, no_reply?: false) == {:ok, :set}
      assert HashManager.set_new(:cars, car, no_reply?: false) == {:ok, :not_changed}
      assert HashManager.get(:cars, [car.brand, car.model]) == {:ok, car}
    end

    test "fails if key data does not exist" do
      Car.create(0)
      invalid_car = Map.delete(Car.get(1), :brand)
      assert {:error, %Error{} = error} = HashManager.set_new(:cars, invalid_car)
      assert error.reason == :key_not_found
      assert Error.message(error) == "data does not have key 'brand'"
      assert Error.message_raw(error, format: :python) == "data does not have key '%(key)s'"
    end

    test "fails if data cannot be encoded to JSON" do
      Car.create(0)
      invalid_car = Map.put(Car.get(1), :i_will, {:create, :error})
      assert {:error, %Error{} = error} = HashManager.set_new(:cars, invalid_car)
      assert error.reason == :encode_failed
      assert Error.message(error) == "data cannot be converted to JSON"
    end
  end
end
