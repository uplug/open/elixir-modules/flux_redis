defmodule FluxRedis.PipelineManagerTest do
  use ExUnit.Case

  alias FluxRedis.{Error, PipelineManager}
  alias FluxRedis.HashManager.Commands
  alias FluxRedisTest.EnvManager
  alias FluxRedisTest.Factories.Car
  alias FluxRedisTest.RedisManager

  setup_all do
    RedisManager.connect()
  end

  setup do
    on_exit(fn ->
      RedisManager.reset()
      EnvManager.reset()
    end)
  end

  describe "PipelineManager.pipeline!/2" do
    test "succeeds if all commands succeed" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      results = [:set, get_car, :ok]

      assert PipelineManager.pipeline!(commands) == results
    end

    test "succeeds if sending succeed (no_reply)" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      assert PipelineManager.pipeline!(commands, no_reply?: true) == :ok
    end

    test "succeeds if all commands succeed (no_transaction)" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      results = [:set, get_car, :ok]

      assert PipelineManager.pipeline!(commands, as_transaction?: false) == results
    end

    test "fails if any command fail" do
      get_car_not_found = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car_not_found.brand, get_car_not_found.model]),
        Commands.delete(:cars, [set_car.brand, set_car.model])
      ]

      error = assert_raise Error, fn -> PipelineManager.pipeline!(commands) end
      assert error.reason == :not_found
      assert Error.message(error) == "data not found"
    end

    test "fails if invalid command set" do
      RedisManager.add_key(:cars, "i own this key")

      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      error = assert_raise Error, fn -> PipelineManager.pipeline!(commands) end
      assert error.reason == :request_failed

      assert error.metadata.detail.message ==
               "WRONGTYPE Operation against a key holding the wrong kind of value"
    end

    test "fails if request timed out" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      error = assert_raise Error, fn -> PipelineManager.pipeline!(commands, timeout: 0) end
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end

  describe "PipelineManager.pipeline/2" do
    test "succeeds if all commands succeed" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      results = [:set, get_car, :ok]

      assert PipelineManager.pipeline(commands) == {:ok, results}
    end

    test "succeeds if sending succeed (no_reply)" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      assert PipelineManager.pipeline(commands, no_reply?: true) == :ok
    end

    test "succeeds if all commands succeed (no_transaction)" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      results = [:set, get_car, :ok]

      assert PipelineManager.pipeline(commands, as_transaction?: false) == {:ok, results}
    end

    test "fails if any command fail" do
      get_car_not_found = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car_not_found.brand, get_car_not_found.model]),
        Commands.delete(:cars, [set_car.brand, set_car.model])
      ]

      assert {:error, %Error{} = error} = PipelineManager.pipeline(commands)
      assert error.reason == :not_found
      assert Error.message(error) == "data not found"
    end

    test "suceeds if invalid command set (array of errors)" do
      RedisManager.add_key(:cars, "i own this key")

      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      assert {:ok, [error, error, error]} = PipelineManager.pipeline(commands)
      assert %Error{} = error
      assert error.reason == :request_failed

      assert error.metadata.detail.message ==
               "WRONGTYPE Operation against a key holding the wrong kind of value"
    end

    test "fails if request timed out" do
      Car.create(1)

      get_car = Car.get(1)
      set_car = Car.get(2)

      commands = [
        Commands.set!(:cars, set_car),
        Commands.get(:cars, [get_car.brand, get_car.model]),
        Commands.delete(:cars, [get_car.brand, get_car.model])
      ]

      assert {:error, %Error{} = error} = PipelineManager.pipeline(commands, timeout: 0)
      assert error.reason == :request_failed
      assert error.metadata.detail.reason == :timeout
    end
  end
end
