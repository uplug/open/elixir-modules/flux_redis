defmodule FluxRedisTest.Factories.Car do
  @moduledoc false

  alias FluxRedis.HashManager
  alias FluxRedisTest.EnvManager

  @brands ~w(Ford Fiat Audi Dodge Citroen VW Jeep Kia Honda Toyota)
  @models ~w(Ka Uno A1 Ram C3 Gol Cherokee K9 City Corolla)
  @years 1990..2020
  @cars for brand <- @brands,
            year <- @years,
            model <- @models,
            do: %{brand: brand, model: model, year: year}
  @cars_length length(@cars)

  def create(size \\ 1) do
    EnvManager.set_hash_storages(cars: [:brand, :model])

    cond do
      size < 1 -> :ok
      size == 1 -> set!(get(1))
      true -> set_many!(get_many(size))
    end
  end

  def get(index) do
    Enum.at(@cars, rem(index - 1, @cars_length))
  end

  def get_many(size) do
    cond do
      size == 1 -> Enum.at(@cars, 0)
      size > @cars_length -> @cars ++ get_many(size - @cars_length)
      size == @cars_length -> @cars
      true -> Enum.slice(@cars, 0, size)
    end
  end

  def set!(car) do
    HashManager.set!(:cars, car)
    car
  end

  def set_many!(cars) do
    HashManager.set_many!(:cars, cars)
  end
end
