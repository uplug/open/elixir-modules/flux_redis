defmodule FluxRedisTest.Factories.User do
  @moduledoc false

  alias FluxRedis.HashManager
  alias FluxRedisTest.EnvManager

  @ages 10..99
  @names ~w(Emma Liam Ava Isabella Noah James Mia Amelia Harper Evelyn)
  @surnames ~w(Smith Long King Jones Brown Davis Miller Wilson Hill Bell)
  @users for name <- @names,
             surname <- @surnames,
             age <- @ages,
             do: %{name: name, surname: surname, age: age}
  @users_length length(@users)

  def create(size \\ 1) do
    EnvManager.set_hash_storages(users: :id)

    cond do
      size < 1 -> :ok
      size == 1 -> set!(get(1))
      true -> set_many!(get_many(size))
    end
  end

  def get(id) do
    add_id(Enum.at(@users, rem(id - 1, @users_length)), id)
  end

  def get_many(size) do
    cond do
      size == 1 -> add_id(Enum.at(@users, 0))
      size > @users_length -> add_ids(@users ++ get_many(size - @users_length))
      size == @users_length -> add_ids(@users)
      true -> add_ids(Enum.slice(@users, 0, size))
    end
  end

  def set!(user) do
    HashManager.set!(:users, user)
    user
  end

  def set_many!(users) do
    HashManager.set_many!(:users, users)
    users
  end

  def add_ids(users) do
    users
    |> Enum.zip(1..length(users))
    |> Enum.map(fn {user, id} -> add_id(user, id) end)
  end

  defp add_id(user, id \\ 1) do
    Map.put(user, :id, id)
  end
end
