defmodule FluxRedisTest.RedisManager do
  @moduledoc false

  def add_key(key, value) do
    Redix.command(:redis, ["SET", key, value])
  end

  def add_hash_element(hash, key, data) do
    Redix.command(:redis, ["HSET", hash, key, data])
  end

  def connect do
    Supervisor.start_link([FluxRedis.child_spec()], strategy: :one_for_one)
    :ok
  end

  def reset do
    Redix.command!(:redis, ~w(FLUSHALL))
  end
end
