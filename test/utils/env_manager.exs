defmodule FluxRedisTest.EnvManager do
  @moduledoc false

  @default_env [
    connection: [
      uri: "redis://redis",
      timeout: 5_000,
      sync_connect: true,
      exit_on_disconnection: false,
      backoff_initial: 1_000,
      backoff_max: 10_000
    ],
    hash_storages: []
  ]

  def reset do
    Application.put_all_env(flux_redis: @default_env)
  end

  def set_hash_storages(hash_storages) do
    Application.put_env(:flux_redis, :hash_storages, hash_storages)
  end
end
