ExUnit.configure(exclude: [pending: true])

ExUnit.start()

require_files = fn path, dir_handler, file_handler ->
  path
  |> File.ls!()
  |> Enum.each(fn file ->
    file
    |> Path.expand(path)
    |> file_handler.(dir_handler, file_handler)
  end)
end

require_file = fn file, dir_handler, file_handler ->
  if File.dir?(file) do
    dir_handler.(file, dir_handler, file_handler)
  else
    Code.require_file(file)
  end
end

require_files.("./test/utils", require_files, require_file)
